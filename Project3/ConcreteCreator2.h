#pragma once
class ConcreteCreator2 : public Creator {
public:
	Product* FactoryMethod() const override {
		return new ConcreteProduct2();
	}
};