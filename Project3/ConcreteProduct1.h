#pragma once
#include <iostream>
#include "Product.h"
class ConcreteProduct1 : public Product {
public:
	std::string Operation() const override {
		return "{Result of the ConcreteProduct1 (changed!!!) }";
	}
};